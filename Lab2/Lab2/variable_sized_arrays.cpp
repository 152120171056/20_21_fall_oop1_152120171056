#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int n, q, k, temp;
    cin >> n >> q;
    vector<int> A;
    vector<vector<int>> B;
    for (int i = 0; i < n; i++) {
        cin >> k;
        for (int j = 0; j < k; j++) {
            cin >> temp;
            A.push_back(temp);
        }
        B.push_back(A);
        A.clear();
    }
    for (int i = 0; i < q; i++) {
        int row, col;
        cin >> row >> col;
        cout << B[row][col] << endl;
    }
    return 0;
}
