#include<iostream>
#include<stdlib.h>
#include<fstream>
#include <string>
using namespace std;
/*! \mainpage  LAB-1
 *  \brief     Read number in the file and performs various operations.
 *  \details   This code was written to calculate the sum, product, average and find the smallest number in given numbers in the txt file.
 *  \author    Yasemin Gerboga (152120171056)
 *	\version   1.0v
 *  \date      1 November 2020 Sunday
 *  \pre       First initialize the system.
 *  \warning   Improper use can crash your application
 *  \copyright GNU Public License.
 */

 /**
 * @brief					: This function is used to calculate sum of numbers which is placed in the txt file.
 * @param numbers			: Numbers to sum in the file
 * @return sum				: Sum of the numbers
 */
float Sum(float* numbers) {
	float sum = 0;
	for (int i = 1; i < numbers[0] + 1; i++)
	{
		sum += numbers[i];
	}
	return sum;
}
/**
* @brief					: This function is used to calculate product of numbers which is placed in the txt file.
* @param numbers			: Numbers to product in the file
* @return product			: Product of numbers
*/
float Product(float* numbers) {
	float product = 1;
	for (int i = 1; i < numbers[0] + 1; i++)
	{
		product *= numbers[i];
	}
	return product;
}
/**
* @brief					: This function is used to calculate average of numbers which is placed in the txt file.
* @param numbers			: Numbers to calculate average in the file
* @return average			: Average of numbers
*/
float Average(float* numbers) {
	float sum = Sum(numbers);
	return sum / numbers[0];
}
/**
* @brief					: This function is used to find smallest of numbers which is placed in the txt file.
* @param numbers			: Numbers to find smallest number in the file
* @return min				: Smallest one of the numbers
*/
float smallestNumber(float* numbers) {
	float min = numbers[1];
	int i = 2;
	for (int i = 0; i < numbers[0] + 1; i++)
	{
		if (numbers[i] < min)
			min = numbers[i];
	}
	return min;
}
/**
* @brief					: This function is main function.
*/
void main() {
	ifstream data;
	float x;
	float* numbers;
	int i = 0;
	x = INT16_MIN;
	data.open("input.txt");
	if (data.is_open())
	{
		data >> x;
		if (x == INT16_MIN) {
			cout << "File is empty." << endl;
			exit(0);
		}
		numbers = new float[x];
		numbers[0] = x;
		i++;
		while (!data.eof()) {
			data >> x;
			numbers[i] = x;
			i++;
		}
		if (i - 1 < numbers[0]) {
			cout << "There are not enough numbers in the array." << endl;
			exit(0);
		}
		if (i - 1 > numbers[0])
		{
			cout << "There are too many numbers in the array." << endl;
			exit(0);
		}
		cout << "Sum is: " << Sum(numbers) << "\n";
		cout << "Product is: " << Product(numbers) << "\n";
		cout << "Average is: " << Average(numbers) << "\n";
		cout << "Smallest number is: " << smallestNumber(numbers) << "\n";
	}
	else cout << "Unable to open file";
	system("pause");
}